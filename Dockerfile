FROM php:8.2-apache

# Install system dependencies
RUN apt-get update && apt-get install -y git unzip libxml2-dev libonig-dev libzip-dev default-mysql-client

# Install PHP extensions individually
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install xml
RUN apt-get update && apt-get install -y \
    git \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    # Instalar y configurar extensiones PHP
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install zip \
    # Habilitar módulos de Apache
    && a2enmod rewrite \
    && a2enmod headers \
    # Limpiar caché de APT
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN pecl install pcov && docker-php-ext-enable pcov
RUN pecl install xdebug && docker-php-ext-enable xdebug
# Instalar Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"

COPY composer.json /var/www/html/

RUN composer install --no-interaction --no-plugins --no-scripts --prefer-dist
RUN composer dump-autoload --optimize

EXPOSE 80
